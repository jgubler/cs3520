package main

import (
	"log"
	"os"
	"strconv"
)

func Not(in chan bool) (out chan bool) {
	out = make(chan bool, 1)
	go func() {
		for {
			out <- !<-in
		}
	}()
	return
}

func And(a, b chan bool) (out chan bool) {
	out = make(chan bool, 2)
	go func() {
		for {
			A := <-a
			B := <-b
			out <- (A && B)
		}
	}()
	return
}

func Nand(a, b chan bool) (out chan bool) {
	out = make(chan bool, 2)
	go func() {
		for {
			A := <-a
			B := <-b
			out <- (!(A && B))
		}
	}()
	return
}

func Or(a, b chan bool) (out chan bool) {
	out = make(chan bool, 2)
	go func() {
		for {
			A := <-a
			B := <-b
			out <- (A || B)
		}
	}()
	return
}

func Nor(a, b chan bool) (out chan bool) {
	out = make(chan bool, 2)
	go func() {
		for {
			A := <-a
			B := <-b
			out <- (!(A || B))
		}
	}()
	return
}

func Xor(a, b chan bool) (out chan bool) {
	out = make(chan bool, 2)
	go func() {
		for {
			A := <-a
			B := <-b
			var result bool
			if (A && !B) || (!A && B) {
				result = true
			} else {
				result = false
			}
			out <- result
		}
	}()
	return
}

func Split2(in chan bool) (out1, out2 chan bool) {
	out1 = make(chan bool, 2)
	out2 = make(chan bool, 2)
	go func() {
		for {
			In := <-in
			out1 <- In
			out2 <- In
		}
	}()
	return
}

func Split3(in chan bool) (out1, out2, out3 chan bool) {
	out1 = make(chan bool, 2)
	out2 = make(chan bool, 2)
	out3 = make(chan bool, 2)
	go func() {
		for {
			In := <-in
			out1 <- In
			out2 <- In
			out3 <- In
		}
	}()
	return
}

func HalfAdder(a, b chan bool) (sum, cout chan bool) {
	A1, A2 := Split2(a)
	B1, B2 := Split2(b)
	sum = Xor(A1, B1)
	cout = And(A2, B2)
	return
}

func FullAdder(a, b, cin chan bool) (sum, cout chan bool) {
	A1, A2 := Split2(a)
	B1, B2 := Split2(b)
	Cin1, Cin2 := Split2(cin)

	Xor1Out := Xor(A1, B1)
	Xor1Out1, Xor1Out2 := Split2(Xor1Out)

	sum = Xor(Xor1Out1, Cin1)

	And1Out := And(Xor1Out2, Cin2)
	And2Out := And(A2, B2)

	cout = Or(And1Out, And2Out)
	return
}

func Add32(a, b []chan bool) (sum []chan bool) {
	sum = make([]chan bool, 32)
	leastbitcarry := make(chan bool, 1)
	biggestbitcarry := make(chan bool, 1)
	var carryDelete bool
	go func() {
		for {
			carryDelete = <-biggestbitcarry
		}
	}()
	sum[0], leastbitcarry = HalfAdder(a[0], b[0])
	for i := 1; i < 31; i++ {
		sum[i], leastbitcarry = FullAdder(a[i], b[i], leastbitcarry)
	}
	sum[31], biggestbitcarry = FullAdder(a[31], b[31], leastbitcarry)
	return
}

func Sum32(x, y uint32) uint32 {
	A := make([]chan bool, 32)
	B := make([]chan bool, 32)
	for i, _ := range A {
		A[i] = make(chan bool, 1)
		B[i] = make(chan bool, 1)
	}

	sum := Add32(A, B)
	for bit := uint32(0); bit < 32; bit++ {
		A[bit] <- (x & (1 << bit)) != 0
		B[bit] <- (y & (1 << bit)) != 0
	}
	total := uint32(0)
	for bit := uint32(0); bit < 32; bit++ {
		if <-sum[bit] {
			total += 1 << bit
		}
	}
	return total
}

func main() {
	if len(os.Args) != 3 {
		log.Fatalf("Wrong number of arguments used: adder.go num1 num2")
	}
	num1, err1 := strconv.Atoi(os.Args[1])
	if err1 != nil {
		log.Fatalf("the first number is not an integer")
	}
	num1u := uint32(num1)
	num2, err2 := strconv.Atoi(os.Args[2])
	if err2 != nil {
		log.Fatalf("the second number is not an integer")
	}
	num2u := uint32(num2)

	sum := Sum32(num1u, num2u)
	log.Printf("%v + %v = %v", num1, num2, sum)

}
