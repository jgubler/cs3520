change(e,w).
change(w,e).

move([X,X,M3,C1,C2,C3],m12,[Y,Y,M3,C1,C2,C3]) :-
	change(X,Y).
move([X,M2,X,C1,C2,C3],m13,[Y,M2,Y,C1,C2,C3]) :-
	change(X,Y).
move([M1,X,X,C1,C2,C3],m23,[M1,Y,Y,C1,C2,C3]) :-
	change(X,Y).
move([M1,M2,M3,X,X,C3],c12,[M1,M2,M3,Y,Y,C3]) :-
	change(X,Y).
move([M1,M2,M3,X,C2,X],c13,[M1,M2,M3,Y,C2,Y]) :-
	change(X,Y).
move([M1,M2,M3,C1,X,X],c23,[M1,M2,M3,C1,Y,Y]) :-
	change(X,Y).
move([X,M2,M3,X,C2,C3],m1c1,[Y,M2,M3,Y,C2,C3]) :-
	change(X,Y).
move([X,M2,M3,C1,X,C3],m1c2,[Y,M2,M3,C1,Y,C3]) :-
	change(X,Y).
move([X,M2,M3,C1,C2,X],m1c3,[Y,M2,M3,C1,C2,Y]) :-
	change(X,Y).
move([M1,X,M3,X,C2,C3],m2c1,[M1,Y,M3,Y,C2,C3]) :-
	change(X,Y).
move([M1,X,M3,C1,X,C3],m2c2,[M1,Y,M3,C1,Y,C3]) :-
	change(X,Y).
move([M1,X,M3,C1,C2,X],m2c3,[M1,Y,M3,C1,C2,Y]) :-
	change(X,Y).
move([M1,M2,X,X,C2,C3],m3c1,[M1,M2,Y,Y,C2,C3]) :-
	change(X,Y).
move([M1,M2,X,C1,X,C3],m3c2,[M1,M2,Y,C1,Y,C3]) :-
	change(X,Y).
move([M1,M2,X,C1,C2,X],m3c3,[M1,M2,Y,C1,C2,Y]) :-
	change(X,Y).

oneEq(X,X,X,X,X,X).

oneEq(X,X,Y,X,X,Y).
oneEq(X,Y,X,X,X,Y).
oneEq(Y,X,X,X,X,Y).

oneEq(X,X,Y,X,Y,X).
oneEq(X,Y,X,X,Y,X).
oneEq(Y,X,X,X,Y,X).

oneEq(X,X,Y,Y,X,X).
oneEq(X,Y,X,Y,X,X).
oneEq(Y,X,X,Y,X,X).
	
safe([M1,M2,M3,C1,C2,C3]) :-
	oneEq(M1,M2,M3,C1,C2,C3).

	
solution([e,e,e,e,e,e],[]).
solution(Config,[Move|Rest]) :-
	move(Config,Move,NextConfig)
	safe(NextConfig),
	solution(NextConfig,Rest).