"guess game is awesome"
high := 1000.
low := 1.
tries := 1.
triesStr := 1 storeString.

Transcript show: 'Pick a number between 1 and 1000 and I will try to guess it
in no more than 10 tries.  After each guess, enter 0 if I
got it right, -1 if I need to guess lower, and 1 if I need to
guess higher.'; cr.

[(high > low)&(tries < 11)]
whileTrue: [
    guess := ((high + low)//2).
    highStr := guess storeString.
    Transcript show: ('My guess is ',highStr,'.  Please enter -1, 0, or 1:').
    userSays := stdin nextLine asInteger.
    (userSays = 0) ifTrue: [
        Transcript show: ('That took ',triesStr,' guesses.'); cr.
    ].
    (userSays = 1) ifTrue: [
        low := (guess).
    ].
    (userSays = -1) ifTrue: [
        high := (guess).
    ].
    tries := tries + 1.
    triesStr := tries storeString.
].

(high = low) ifTrue: [
    Transcript show: ('That took ',triesStr,' guesses.'); cr.
].
