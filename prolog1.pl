parent(alli,reese).
parent(tony,reese).
parent(wendell,alli).
parent(wendell,kim).
parent(norman,wendell).
parent(mindy,daniel).
parent(susan,mindy).
parent(kevin,mindy).
parent(linda,alli).
parent(linda,james).
parent(james,daniel).
parent(wendell,james).

female(alli).
female(kim).
female(mindy).
female(susan).
female(linda).

male(tony).
male(wendell).
male(norman).
male(kevin).
male(james).
male(reese).
male(daniel).

mother(M,C) :-
  parent(M,C),
  female(M).
/* mother(linda,james). should be true
   mother(kevin,mindy). should be false */

father(F,C) :-
  parent(F,C),
  male(F).
/* father(wendell,james). true
   father(mindy,daniel). false */

sister(X,Y) :-
  parent(P,X),
  parent(P,Y),
  female(X),
  not(=(X,Y)).
/* sister(kim,alli). true
   sister(mindy,kim). false
   sister(kim, james). true*/

grandson(GS,GP) :-
  parent(P,GS),
  parent(GP,P),
  male(GS).
/* grandson(daniel,wendell). true
   grandson(daniel,kevin). true
   grandson(wendell,james). false */

firstCousin(X,Y) :-
 parent(PX,X),
 parent(PY,Y),
 parent(GP,PX),
 parent(GP,PY),
 not(=(PX,PY)),
 not(=(X,Y)).
/* firstCousin(daniel,reese). true
   firstCousin(kim,james). false*/

descendent(X,Y) :- parent(Y,X).
descendent(X,Y) :-
 parent(Y,Z),
 descendent(X,Z).
/* descendent(daniel,wendell). true
descendent(james,kevin). false*/

third(X,Y) :-
 X = .(A,.(B,.(Y,C))).
/* third([1,2,3], Y). 
   third([1,2],Y).
   third([1],Y).
   third([1,3,4,2,5,234,6,53,4]).
*/

del3(X,Y) :-
 X = .(A,.(B,.(C,D))),
 Y = [A,B|D].
/* del3([1],Y).
   del3([7,2],Y).
   del3([2,4,6],Y).
   del3([2,4,1,3,432,6,8,6,9],Y).
*/

isDuped(Y) :- Y = [A,A].
isDuped(Y) :- 
 Y = [A,A|B],
 isDuped(B).
/* isDuped([7]).
   isDuped([7,7]).
   isDuped([9,7]).
   isDuped([9,9,3,3,6,6,8,8,9,9]).
   isDuped([9,9,3,3,7,8]).
   isDuped([1,1,3]).
*/

evenSize(X) :- X = [A,B].
evenSize(X) :- 
 X = [A,B|C],
 evenSize(C).
/* evenSize([1,3]).
   evenSize([1]).
   evenSize([2,2,3,45,3,4]).
   evenSize([1,1,1,1,1,1,2,3,4,3,9]).
*/
