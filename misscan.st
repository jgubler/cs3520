"Missionaries and Cannibals, the small talk sequel"

Object subclass: State [
    | misNum misNear misFar canNum canNear canFar boatSide boatNum history |
    missionaries: m mNear: mn cNear: cn cannibals: c boatNum: n boat: side hist: h [
        misNum := m.
        canNum := c.
        boatNum := n.
        misNear := mn.
        canNear := cn.
        boatSide := side.
        misFar := (misNum - misNear).
        canFar := (canNum - canNear).
        history := h.
    ]
    isGoal [
        ((misNear = 0) and: [canNear = 0]) ifTrue: [
            ^true.
        ] ifFalse: [
            ^false.
        ] 
    ]
    oneMove [
        | moves moveQueue m c b s mSize counter tempHistory myState |
        moveQueue := OrderedCollection new.
        tempHistory := history.
        myState := State new.
        myState missionaries: misNum mNear: misNear cNear: canNear cannibals: canNum boatNum: boatNum boat: boatSide hist: tempHistory.
        tempHistory add: myState.
        m := 0.
        c := 0.
        b := boatSide * -1.
        (boatSide = -1) ifTrue: [
            0 to: misNum do: [:m|
                0 to: canNum do: [:c|
                    (((m+c) = 0) | ((m+c) > boatNum)) ifFalse: [
                        s := State new.
                        s missionaries: misNum mNear: (misNear - m) cNear: (canNear - c) cannibals: canNum boatNum: boatNum boat: b hist: tempHistory.
                    moveQueue add: s
                    ].
                ].
            ].
        ] ifFalse: [  
            0 to: misNum do: [:m|
                0 to: canNum do: [:c|
                    (((m+c) = 0) | ((m+c) > boatNum)) ifFalse: [
                        s := State new.
                        s missionaries: misNum mNear: (misNear + m) cNear: (canNear + c) cannibals: canNum boatNum: boatNum boat: b hist: tempHistory.
                    moveQueue add: s
                    ].
                ].
            ].
        ].
        ^moveQueue.
    ]
    isValid [
        ((((misNear between: 0 and: misNum) and: [misFar between: 0 and: misNum]) and: [canNear between: 0 and: canNum]) and: [canFar between: 0 and: canNum]) ifTrue: [
            ^true.
        ] ifFalse: [
            ^false.
        ]
    ]
    isSafe [
        (((misNear >= canNear)|(misNear = 0)) & ((misFar >= canFar)|(misFar = 0))) ifTrue: [
            ^true.
        ] ifFalse: [
            ^false.      
        ] 
    ]
    makeString[
        | stateString |
        stateString := '(',(misNear storeString),' ',(canNear storeString),' ',(boatSide storeString),')',' \|\ ','(',((-1*boatSide) storeString),' ',(misFar storeString),' ',(canFar storeString),')'.
        ^stateString.
    ]
    returnHistory[
        ^history.
    ]
]

Object subclass: Solver [
    | initialState tempQueue queue history duplicates tempState possibleMoves possMovesSize tempPossMove dupString initString checkState checkStateQueue |
    missionaries: m cannibals: c boat: n [
        | initHistory |
        initHistory := OrderedCollection new.
        history := OrderedCollection new.
        queue := OrderedCollection new.
        duplicates := Set new.
        initialState := State new.
        initialState missionaries: m mNear: m cNear: c cannibals: c boatNum: n boat: -1 hist: initHistory.
        queue add: initialState. 
        
        initString := initialState makeString.
        duplicates add: initString.
        [(queue size)>0]
        whileTrue: [
            tempState := queue first.
            queue removeFirst.
            (tempState isGoal = true) ifTrue: [
                history := tempState returnHistory.
                history add: tempState.
                self printSolution: history.
                ^history.
            ] ifFalse: [
                possibleMoves := tempState oneMove.
                possMovesSize := possibleMoves size.
                [(possibleMoves size)>0]
                whileTrue: [
                    tempPossMove := possibleMoves first.
                    possibleMoves removeFirst.
                    dupString := tempPossMove makeString.
                    (((tempPossMove isValid) = true) & ((tempPossMove isSafe) = true) &((duplicates includes: dupString)=false))ifTrue: [
                        duplicates add: dupString.
                        queue add: tempPossMove.
                    ].
                ].
            ].
        ].
    ]
    printSolution: sol [
        | solution tempString |
        solution := sol.
        solution do: [:x |
            tempString := x makeString.
            Transcript show: tempString; cr.
        ].
    ]
]



s := Solver new.
tim := s missionaries: 3 cannibals: 3 boat: 2.

t := Solver new.
bob := t missionaries: 5 cannibals: 5 boat: 3.

