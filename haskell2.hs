-- A datatype for Prolog terms. A term can be an Atom, a Variable,
-- or a Compound term. We represent all the lexemes using strings.

data Term = Atom String | Variable String | Compound String [Term]

-- Pretty-print a term

instance Show Term where show t = showTermList [t]


-- Pretty-print a list of terms

showTermList :: [Term] -> String
showTermList []              = ""
showTermList [Atom s]        = s
showTermList [Variable n]    = n
showTermList [Compound n tl] = n ++ "(" ++ showTermList tl ++ ")"
showTermList (term:rest)     = showTermList [term] ++ "," ++ showTermList rest


-- A special mapping function for terms. We convert a term into the
-- new term formed by applying the function f to all the variables
-- (but leaving all the atoms and predicate names unaltered). This
-- can be used for applying a substitution to a term, for example.

mapVariable :: (String -> Term) -> Term -> Term
mapVariable f (Variable n)       = f n
mapVariable f (Compound n terms) = Compound n (map (mapVariable f) terms)
mapVariable _ t                  = t


-- Create a substitution. We take a variable name and a term, and
-- return a function that can be used to apply that substitution to
-- any term.

sub :: String -> Term -> Term -> Term
sub name term = mapVariable (\n -> if n == name then term else Variable n)


-- Find a most general unifier of two terms. We return a pair of
-- values, like this:
--
--     let (unifiable, unifier) = mgu a b
--
-- The first value we return is a boolean, true iff the two terms
-- are unifiable. If they are, the second value we return is a
-- most general unifier: a function that, when applied to a and b,
-- makes them identical. We do not perform the occurs check.

mgu :: Term -> Term -> (Bool, Term -> Term)
mgu a b = ut [a] [b] (\x -> x)
          where
              ut [] [] unifier = (True, unifier)
              ut (term : t1) (Variable name : t2) unifier =
                  let r = (sub name term) . unifier
                  in ut (map r t1) (map r t2) r
              ut (Variable name : t1) (term : t2) unifier =
                  let r = (sub name term) . unifier
                  in ut (map r t1) (map r t2) r
              ut (Atom n : t1) (Atom m : t2) unifier
                  | n == m = ut t1 t2 unifier
                  | otherwise = (False, unifier)
              ut (Compound n1 xt1 : t1) (Compound n2 xt2 : t2) unifier
                  | n1 == n2 && length xt1 == length xt2 = ut (xt1 ++ t1) (xt2 ++ t2) unifier
                  | otherwise = (False, unifier)
              ut _ _ unifier = (False, unifier)


-- Challenge problem number one. A program with four clauses
-- and a query. This one does not require variable renaming.

-- 1. p(f(Y)) :- q(Y), r(Y).
c1 :: [Term]
c1 = [Compound "p" [Compound "f" [Variable "Y"]],
      Compound "q" [Variable "Y"],
      Compound "r" [Variable "Y"]]

-- 2. q(g(Z)).
c2 :: [Term]
c2 = [Compound "q" [Compound "g" [Variable "Z"]]]

-- 3. q(h(Z)).
c3 :: [Term]
c3 = [Compound "q" [Compound "h" [Variable "Z"]]]

-- 4. r(h(a)).
c4 :: [Term]
c4 = [Compound "r" [Compound "h" [Atom "a"]]]

-- The program
program :: [[Term]]
program = [c1, c2, c3, c4]

-- The query: p(X).
query :: Term
query = Compound "p" [Variable "X"]

-- To test with this problem, call: prolog program query

-- i couldn't figure out the renaming, so there are nums passed around, but they don't do anything anymore

resolution :: (Term -> Term) -> [Term] -> [Term] -> Term -> ([Term], Term)
resolution subf cs gs query = (map (subf) gs ++ map (subf) cs, subf query)
    
solve :: [[Term]] -> [Term] -> Term -> Integer -> [Term]
solve _ [] query _ = [query]
solve clauses@(xs:ys) goals@(g:gs) query num = helper program program goals query num
    
-- solve's helper class to take over the recursion
helper :: [[Term]] -> [[Term]] -> [Term] -> Term -> Integer -> [Term]
helper _ [] _ _ _ = []
helper _ _ [] _ _ = []
helper program newprog@(as@(c:cs):bs) goals@(g:gs) query num
    | unifiable == True = solve program newgoals newquery (num+1) ++ helper program bs goals query num
    | otherwise = helper program bs goals query num
    where
        (unifiable, subfunction) = mgu c g
        (newgoals,newquery) = resolution subfunction cs gs query


prolog :: [[Term]] -> Term -> [Term] 
prolog clauses@(xs:ys) query = solve clauses [query] query 0

-- prolog program [query] query


