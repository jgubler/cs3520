module Main where
data Tree a = Leaf | Branch a (Tree a) (Tree a) deriving (Show, Eq)

sumList [] = 0
sumList (x:xs) = sumList xs + x
--sumList [1,2,3,4]

sumList2 :: [Integer] -> Integer
sumList2 (x:xs) = foldl (\a b -> a + b)x xs
--sumList2 [1,2,3,4]

productList :: [Integer] -> Integer
productList (x:xs) = foldr (\a b -> a * b)x xs
--productList [1,2,3,4]

appendList :: [a] -> [a] -> [a]
appendList xs [] = xs
appendList [] ys = ys
appendList (x:xs) (y:ys) = x:y:foldr (\ d e -> d:e)xs ys
--appendList [1,2,5,3] [4,7,8,9]

quicksort :: Ord a => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = smallerThan ++ [x] ++ biggerThan
	where smallerThan = quicksort [a| a <-xs, a <= x]
	      biggerThan = quicksort [a| a<-xs , a > x]
--quicksort [2,4,5,2,5,3,4,87,7,5]

dropN :: [a] -> Integer -> [a]
dropN xs n = dropper xs n
	where dropper [] _ = []
	      dropper (x:xs) 1 = dropper xs n
	      dropper (x:xs) num = x : dropper xs (num-1)
--dropN 3 [2,4,5,2,5,3,4,87,7,5]

compress :: Eq a => [a] -> [(Integer, a)]
compress [] = []
compress xs = repeats xs 1
    where 
        repeats [] _ = []
        repeats (x:y) num
	        |rest == [] = [(num,x)]
            |x == z = repeats rest (num+1)
            |otherwise = (num,x):acc
            where
                rest = y
                (z:zs) = y
                acc = repeats rest 1
-- compress " aaaddddddrfffffffff"

inorder :: Ord a => Tree a -> Bool
inorder (Branch n Leaf Leaf) = True
inorder (Branch n l@(Branch ln _ _ ) Leaf) = n > ln
inorder (Branch n Leaf r@(Branch rn _ _)) = n < rn
inorder (Branch n l@(Branch ln _ _ ) r@(Branch rn _ _))  
    | n > rn = False
    | n < ln = False
    | inorder l && inorder r == False = False
    | otherwise = True

t = Branch 5 (Branch 4 Leaf Leaf) Leaf
t1 = Branch 5 (Branch 3 (Branch 1 Leaf Leaf) (Branch 4 Leaf Leaf)) (Branch 9 Leaf Leaf)
t2 = Branch 5 (Branch 3 (Branch 1 Leaf Leaf) (Branch 4 Leaf Leaf)) (Branch 9 (Branch 7 Leaf Leaf) (Branch 11 Leaf Leaf))
-- inorder t
-- inorder t1 

atlevel :: Tree a -> Integer -> [a]
atlevel b@(Branch n (Branch ln _ _) (Branch rn _ _)) num = level b 0 num []
    where
        level (Branch n' l Leaf) d max acc
            | d == max = n':acc
            | d < max = (level l (d+1) max acc)
            | otherwise = []
        level (Branch n' Leaf r) d max acc
            | d == max = n':acc
            | d < max = (level r (d+1) max acc)
            | otherwise = []
        level (Branch n' l r) d max acc
            | d == max = n':acc
            | d < max = (level l (d+1) max acc) ++ (level r (d+1) max acc)
            | otherwise = []

-- atlevel t2 0
-- atlevel t2 1
-- atlevel t3 2
-- atlevel t1 0
-- atlevel t1 1
-- atlevel t1 2 thought t2 and above starts to fail, i struggled with an uneven tree on this one like t1



