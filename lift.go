//
// Lift simulator
// CS 3520
//
// Starter code
//

package main

import (
	"log"
	"math/rand"
	"time"
)

// the length of time to pause between floors and when opening doors
var Pause = time.Second

//
// Controller component
//

// A controller component.
// Use a pointer to this struct as your handle to a controller.
// Send a Step message to it by sending a destination floor across
// the Step channel. Only the lift should send this message.
//
// When NewController returns, the Lift field is nil. It should be
// set to a valid Lift before any messages are sent to the
// controller.
//
// The controller assumes that it starts on floor 1 with the motor
// stopped.

type Controller struct {
	Step chan int
	Lift *Lift
}

// Construct a new controller component.
// Returns a handle to the controller, which runs in its own
// goroutine.
func NewController() *Controller {
	controller := &Controller{Step: make(chan int, 1)}
	go func(Cont *Controller) {
		var running bool = false
		var floor int = 1

		var stepValue int
		stop := time.After(Pause)
		<-stop
		for {
			if running == false { 
				stepValue = <-Cont.Step
				if floor < 0 {
                    
					break
				} else if floor > stepValue {
                    
					running = true
					floor -= 1
					stop = time.After(Pause)
				} else if floor < stepValue {
                   
					running = true
					floor += 1
					stop = time.After(Pause)
				} else { // this else says the floor is the same as the stepvalue
                    
				}
			} else {
				<-stop
                running = false
				Cont.Lift.At <- floor}
		}
        log.Printf("should only get here when floor is -1")
	}(controller)
	return controller
}

//
// Lift component
//

// A helper function to append a new element to the end of a slice,
// but only if it does not match the existing last element of the
// slice.
func schedulelast(slice []int, elt int) []int {
    if len(slice) == 0{
        slice = append(slice, elt)
    } else {
	    last := len(slice) - 1
	    if !(slice[last] == elt) {
		    slice = append(slice, elt)
	    }
    }
	return slice
}

// A lift component.
// Use a pointer to this struct as your handle to a lift.

// Send an At message to it by sending a floor number across the At
// channel. This should only be sent by the lift's controller, and
// indicates that the lift has arrived at a new floor.

// Send a Call message to it by sending a new destination floor
// number across the Call channel. This can come from a floor
// component (when someone presses the call button on that floor) or
// directly from a user (when the user is inside the elevator and
// pushes a button).
//
// Send a negative value to Call to shut the lift down and end its
// goroutine.
type Lift struct {
	At   chan int
	Call chan int
}

// Construct a new lift component.
// Returns a handle to the lift, which runs in its own goroutine.
//
// number is the lift number (1-based numbering)
//
// controller is the controller for this lift.
//
// floors is a slice of all of the floors in the building. This is a
// 1-based list, so entry #0 is unused. It is okay to pass in a
// slice of nil values, as long as they are all filled in before
// any messages are sent to the lift.
func NewLift(number int, controller *Controller, floors []*Floor) *Lift {
	lift := &Lift{make(chan int, 1), make(chan int, 1)}
	go func(l *Lift, num int, cont *Controller, floorlist []*Floor) {
		var floor int = 1
		var moving bool = false
		var schedule []int
		for {
			select {
			case destination := <-lift.Call:
				// respond to a Call message
				if destination < 0 {
					break
				} else {
					log.Printf("Lift #%v, Called to Floor %v", num, destination)
					if floor == destination && !moving {
						msgchan := make(chan bool, 1)
						floorlist[destination].Arrive <- msgchan
						<-msgchan
					} else {
						schedule = schedulelast(schedule, destination)
						if !moving {
							cont.Step <- schedule[0]
							moving = true
						}
					}
				}

			case newFloor := <-lift.At:
				// respond to an At message
				log.Printf("Lift #%v, Arrived at Floor %v", num, newFloor)
				if newFloor == schedule[0] {
					msgchan := make(chan bool, 1)
					floorlist[newFloor].Arrive <- msgchan
					<-msgchan
					floor = newFloor
					schedule = schedule[1:]
					if len(schedule) == 0 {
						moving = false
					} else {
						cont.Step <- schedule[0]
						moving = true
					}
				} else {
					floor = newFloor
					cont.Step <- schedule[0]
				}
			}
		}
	}(lift, number, controller, floors)

	return lift
}

//
// Floor component
//

// a convenience type representing an acknowledgement channel
type AckChan chan bool

// the states that a floor can be in
type floorState int

const (
	CALLED    floorState = iota // the floor is waiting for a lift to arrive
	NOTCALLED                   // the floor is idle
	DOORSOPEN                   // at least one lift is at this floor with its doors open
)

// A floor component.
// Use a pointer to this struct as your handle to a floor.
//
// Send a Call message to it by sending a boolean value across the
// Call channel. This should be send by a user, i.e., it can come
// from anywhere.
//
// Send an Arrive message to the lift by sending an AckChan across
// the Arrive channel. This message indicates that a lift has
// arrived as the result of a user call. Its doors are immediately
// opened, and a boolean should be send across the AckChan when they
// are closed again. This message is only sent by lift components.
//
// Send a negative value to Call to shut the floor down and end its
// goroutine.
type Floor struct {
	Call   chan bool
	Arrive chan AckChan
}

// Construct a new floor component.
// Returns a handle to floor, which runs in its own goroutine.
//
// number is the floor number (1 based numbering)
//
// lifts is a slice containing all the lifts in the building. This
// is a 1-based list, so entry #0 is unused. It can contain nil
// values when NewFloor is called, as long as the lift values are
// filled in before any messages are send to the floor.
func NewFloor(number int, lifts []*Lift) *Floor {
	floor := &Floor{make(chan bool, 1), make(chan AckChan, 1)}
	fs := NOTCALLED
	go func(f *Floor, num int, liftlist []*Lift, floorState floorState) {
		stop := time.After(Pause)
		<-stop
		Acknowledges := make([]AckChan, 0, len(liftlist))
		for {
			if floorState == NOTCALLED {
				select {
				    case TellLiftChan := <-f.Arrive:
					    log.Printf("Lift arrived at floor #%v", num)
					    floorState = DOORSOPEN
					    stop = time.After(Pause)
					    Acknowledges = append(Acknowledges, TellLiftChan)
				    case <-f.Call:
					    log.Printf("floor #%v summoning elevator", num)
                        randomlift := rand.Intn(len(liftlist)-2)+1
                        
					    liftlist[randomlift].Call <- num
					    floorState = CALLED
				}
			} else if floorState == CALLED {
				select {
				case TellLiftChan := <-f.Arrive:
					log.Printf("Lift arrived at floor #%v", num)
					stop = time.After(Pause)
					floorState = DOORSOPEN
					Acknowledges = append(Acknowledges, TellLiftChan)
				case <-f.Call:
				}

			} else if floorState == DOORSOPEN {
				select {
				case <-stop:
					log.Printf("DOORS CLOSING on floor #%v", num)
					for _, response := range Acknowledges {
						response <- true
					}
					Acknowledges = make([]AckChan, 0, len(liftlist))
					floorState = NOTCALLED
				case TellLiftChan := <-f.Arrive:
					Acknowledges = append(Acknowledges, TellLiftChan)
				case <-f.Call:
				}
			}
		}
	}(floor, number, lifts, fs)
	return floor
}

//
// Building component
//

// Construct a new building super-component.
// Returns the slice of floors and the slice of lifts, given the
// desired number of each. Note that these slices are 1-based, so
// entry #0 in each is unused.
func NewBuilding(nfloors, nlifts int) (floors []*Floor, lifts []*Lift) {
    floors = make([]*Floor,nfloors+1,nfloors+1)
    lifts = make([]*Lift,nlifts+1,nlifts+1)
    for l := 1; l <= nlifts; l++{
        controller := NewController()
        lift := NewLift(l,controller,floors)
        controller.Lift = lift
        lifts[l] = lift
    }
    for f := 1; f <= nfloors; f++{
        floor := NewFloor(f,lifts)
        floors[f] = floor
    } 
	return
}

func main() {
	rand.Seed(time.Now().UnixNano())
	

	// an example of how to use the components
//	floors, lifts := NewBuilding(10, 2)
//	floors[9].Call <- true
//	time.Sleep(time.Second * 5 / 2)
//	floors[10].Call <- true

//	time.Sleep(time.Second / 2)
//	lifts[1].Call <- 4
//	lifts[2].Call <- 5

//	time.Sleep(100 * time.Second)

    floors, lifts := NewBuilding(30, 7)
	floors[9].Call <- true
	time.Sleep(time.Second * 5 / 2)
	floors[10].Call <- true

	time.Sleep(time.Second / 2)
    floors[30].Call <- true
    time.Sleep(time.Second / 2)
    floors[18].Call <- true
    time.Sleep(time.Second / 2)
    floors[9].Call <- true
    time.Sleep(time.Second / 2)
    floors[27].Call <- true
    time.Sleep(time.Second / 2)
	lifts[1].Call <- 4
	lifts[2].Call <- 5

	time.Sleep(100 * time.Second)
    return
}
