"Binary Search Tree"

Object subclass: Node [
    | left value key right getvalue |
    init: initkey to: initvalue [
        key := initkey.
        value := initvalue.
        left := nil.
        right := nil.
    ]
    insert: newkey to: newvalue [
        (newkey = key) ifTrue: [
            value := newvalue.
        ].
        (newkey < key) ifTrue: [
            (left = nil) ifTrue: [
                left := Node new.
                left init: newkey to: newvalue. 
            ] ifFalse: [
                left insert: newkey to: newvalue.
            ].
        ].
        (newkey > key) ifTrue: [
            (right = nil) ifTrue: [
                right := Node new.
                right init: newkey to: newvalue.
            ] ifFalse: [
                right insert: newkey to: newvalue.
            ].
        ].
    ]
    retrieve: getkey [
        (getkey = key) ifTrue: [
            getvalue := value.
        ].
        (getkey < key) ifTrue: [
            (left = nil) ifTrue: [
                getvalue := nil
            ] ifFalse: [
                getvalue := left retrieve: getkey.
            ].
        ].
        (getkey > key) ifTrue: [
            (right = nil) ifTrue: [
                getvalue := nil
            ] ifFalse: [
                getvalue := right retrieve: getkey.
            ].
        ].
        ^getvalue.
    ]
]

Object subclass: Tree [
    | root value |
    init: key to: value [
        root := Node new.
        root init: key to: value.
    ]
    set: key to: value [
        root insert: key to: value.
    ]
    get: key [
        value := root retrieve: key.
        (value = nil) ifTrue: [
            Transcript show: 'is not in the tree'; cr.
        ] ifFalse: [
            Transcript show: value; cr.
        ].
    ]
]

t1 := Tree new.
t1 init: 10 to: 'bob'.
t1 set: 11 to: 'tree'.
t1 set: 7 to: 'other'.
t1 set: 9 to: 'hot'.
t1 set: 1 to: 'mess'.
t1 set: 15 to: 'ugly'.
t1 set: 14 to: 'bugger'.
t1 set: 27 to: 'dan'.
t1 set: 28 to: 'mindy'.
t1 set: 29 to: 'taco'.
t1 set: 2 to: 'fish'.
t1 set: 3 to: 'green'.
t1 set: 100 to: 'witch'.
t1 get: 9.
t1 get: 10.
t1 get: 6.
t1 set: 9 to: 'not hot'.
t1 get: 9.

t2 := Tree new.
t2 init: 121 to: 'perseus'.
t2 set: 1300 to: 'zeus'.
t2 set: 100 to: 'poseiden'.
t2 set: 101 to: 'hera'.
t2 set: 71 to: 'hades'.
t2 set: 70 to: 'chiron'.
t2 set: 16 to: 'aprhodite'.
t2 set: 11 to: 'grover'.
t2 set: 97 to: 'tyson'.
t2 set: 77 to: 'annabeth'.
t2 set: 82 to: 'thalia'.
t2 get: 1300.
t2 get: 101.
t2 get: 6.
t2 set: 1300 to: 'heracles'.
t2 get: 1300.


